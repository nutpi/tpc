import hilog from "@ohos:hilog";
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@package:pkg_modules/.ohpm/@ohos+hypium@1.0.11/pkg_modules/@ohos/hypium/index";
import ansiRegex from "@bundle:com.example.ohpm_ansiregex/entry_test@library/index";
export default function abilityTest() {
    describe('ActsAbilityTest', () => {
        // Defines a test suite. Two parameters are supported: test suite name and test suite function.
        beforeAll(() => {
            // Presets an action, which is performed only once before all test cases of the test suite start.
            // This API supports only one parameter: preset action function.
        });
        beforeEach(() => {
            // Presets an action, which is performed before each unit test case starts.
            // The number of execution times is the same as the number of test cases defined by **it**.
            // This API supports only one parameter: preset action function.
        });
        afterEach(() => {
            // Presets a clear action, which is performed after each unit test case ends.
            // The number of execution times is the same as the number of test cases defined by **it**.
            // This API supports only one parameter: clear action function.
        });
        afterAll(() => {
            // Presets a clear action, which is performed after all test cases of the test suite end.
            // This API supports only one parameter: clear action function.
        });
        it('assertContain', 0, () => {
            // Defines a test case. This API supports three parameters: test case name, filter parameter, and test case function.
            hilog.info(0x0000, 'testTag', '%{public}s', 'it begin');
            let a = 'abc';
            let b = 'b';
            // Defines a variety of assertion methods, which are used to declare expected boolean conditions.
            expect(a).assertContain(b);
            expect(a).assertEqual(a);
            expect(ansiRegex().test('\u001B[4mcake\u001B[0m')).assertTrue();
            expect(ansiRegex().test('cake')).assertFalse();
            expect(`${'\u001B[4mcake\u001B[0m'.match(ansiRegex())}`
                === `${['\x1B[4m', '\x1B[0m']}`).assertTrue();
            expect(`${'\u001B]8;;https://github.com\u0007click\u001B]8;;\u0007'.match(ansiRegex())}`
                == `${['\x1B]8;;https://github.com\x07', '\x1B]8;;\x07']}`).assertTrue();
        });
    });
}
