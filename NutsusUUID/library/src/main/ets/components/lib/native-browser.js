import { CryptoJS } from '@ohos/crypto-js';
const randomUUID =
  typeof CryptoJS !== 'undefined' && CryptoJS.randomUUID && CryptoJS.randomUUID.bind(CryptoJS);

export default { randomUUID };
