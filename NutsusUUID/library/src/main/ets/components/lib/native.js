import { CryptoJS } from '@ohos/crypto-js';

export default { randomUUID: CryptoJS.randomUUID };
