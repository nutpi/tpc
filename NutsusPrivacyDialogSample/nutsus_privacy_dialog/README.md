# 坚果派隐私协议动态共享库与实例

#### 介绍
隐私协议对话框动态共享库模块

#### 软件架构
使用Shard Library动态共享库实模块现隐私协议对话框和隐私协议显示，对话框使用自定义对话框实现，隐私协议显示在一个Webview组件页面上，支持本地html文件和http或https返回html文件。


#### 安装教程

1.  选择nutsus_privacy_dialog模块, 点击菜单"构建" -> 构建模块"nutsus_privacy_dialog", 在此模块下build -> outputs -> default目录生成har和hsp文件。
2.  har文件通过ohpm安装到使用方上，调试阶段，运行使用方hap文件前，要先安装hsp文件动态共享库, 使用方生成hap文件后，通过命令行安装hsp和hap.

#### 使用说明

1.  复制har文件到使用方项目根目录下，在entry目录下执行`ohpm install ..\nutsus_privacy_dialog.har`安装har文件;
    安装完成后，entry\oh-package.json5
    ```  
    "dependencies": {
    "@nutsus/privacy_dialog": "file:../nutsus_privacy_dialog.har"
    }
    ```
2.  在EntryAbility.ts文件里创建首选项数据库

```
import { CustomDialogPrivacy,PreferencesUtil } from '@nutsus/privacy_dialog'
let preferencesUtil = new PreferencesUtil();

onCreate(want, launchParam) {
    // 创建首选项数据库
    preferencesUtil.createPrivacyPreferences(this.context);
    // 设置隐私协议默认不同意
    preferencesUtil.saveDefaultPrivacy(false);
  }
```

3.  在Index.ets页面，调用对话框

```
import { CustomDialogPrivacy,PreferencesUtil } from '@nutsus/privacy_dialog'
let preferencesUtil = new PreferencesUtil();

  // 开始显示隐私协议对话框
  /**
   * 如果localHtml参数为true,urlPage参数为空，显示默认隐私协议
   * 如果localHtml参数为true,urlPage参数不为空，显示urlPage参数本地html文件
   * 如果localHtml参数为false,urlPage参数为空，显示默认隐私协议
   * 如果localHtml参数为false,urlPage参数不为空，显示urlPage参数http或https返回html文件
   */
  privacyDialogController: CustomDialogController = new CustomDialogController({
    builder: CustomDialogPrivacy({
      localHtml: true,
      urlPage: 'privacy.html'
      // urlPage: 'http://xxxx/zh-cn_userAgreement.html'
    }),
    autoCancel: false,
    alignment: DialogAlignment.Center,
    customStyle: true
  })
  onPageShow() {
    console.info('xx onPageShow 显示隐私协议')
    preferencesUtil.getChangePrivacy().then((value) => {
      console.info(`xx onPageShow 获取隐私协议状态：${value}`)
      if (!value) {
        this.privacyDialogController.open()
      }
    })
  }
  onPageHide() {
    console.info(`xx Index -> onPageHide Close Start`)
    this.privacyDialogController.close()
    console.info(`xx Index -> onPageHide Close End`)
  }
  aboutToDisappear() {
    console.info(`xx Index -> aboutToDisappear`)
    this.privacyDialogController.close()
  }
  // 结束显示隐私协议对话框
```
创建对话框时，可选参数以下：

```
  // 隐私协议标题
  private title?: string = '协议和隐私政策提示'
  // 前辍隐私协议信息
  private prefixMessage?: string = '感谢您选择xxx元服务！我们非常重视您的个人信息和隐私保护。为更好地保障您的个人权益，在使用产品前，请您认真阅读'
  // 后辍隐私协议信息
  private suffixMessage?: string = '的全部内容。'
  // 隐私协议信息，点击可以跳转
  private privacyMessage?: string = '《协议与隐私政策》'
  // 本地html文件或http和https返回html文件
  private localHtml?: boolean = true
  // 隐私协议URL 支持本地html或http和https返回html
  private urlPage?: string = ""
```


4. 安装HSP动态共享库
    在D:\Workspaces\DevecoOpen\NutsusPrivacyDialogSample\nutsus_privacy_dialog\build\default\outputs\default>目录下执行
```
hdc install nutsus_privacy_dialog-default-signed.hsp
```
5. 安装HAP实例项目
    在D:\Workspaces\DevecoOpen\NutsusPrivacyDialogSample\entry\build\default\outputs\default>目录下执行

```
hdc install entry-default-signed.hap
```

6. 点击设备桌面图标运行查看效果
![输入图片说明](screenshots/Screenshot_1.jpeg)
![输入图片说明](screenshots/Screenshot_2.jpeg)
